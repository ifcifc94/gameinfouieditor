package com.ifcifc.gameinfouieditor.Utils;

import java.util.ArrayList;


public class LineClass {
    public String Name = "";
    public int Index = 0;
    public float ScaleX = 1;
    public float ScaleY = 1;
    public boolean useDefaultScale = true;
    public ArrayList<DimensionFormat> Line;

    public LineClass() {
        Line = new ArrayList<>();
    }
    
    public LineClass(String Name, int Index, float ScaleX, float ScaleY, boolean useDefaultScale, ArrayList<DimensionFormat> Line) {
        this.Name = Name;
        this.Index = Index;
        this.ScaleX = ScaleX;
        this.ScaleY = ScaleY;
        this.useDefaultScale = useDefaultScale;
        this.Line = Line;
    }

    public LineClass(String name, int index) {
        this(name, index,1,1, true, new ArrayList<>());
        
        Line.add(new DimensionFormat());
    }

}
