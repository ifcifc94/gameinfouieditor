package com.ifcifc.gameinfouieditor.Utils;

import java.util.ArrayList;


public class DimensionFormat {
    public String         DimensionID = "default";
    public ArrayList<LineFormat>   Format;

    public DimensionFormat() {
        Format = new ArrayList<>();
    }
    
    public DimensionFormat(String dimensionID) {
        DimensionID = dimensionID;
        Format = new ArrayList<>();
    }
}
