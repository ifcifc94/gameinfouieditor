package com.ifcifc.gameinfouieditor.Utils;

public class LineFormat {
    public  boolean isText=true, isDependSep=false;
    public  String Text="", TranslateText="", Separated="", NotSeparated="", Function="", Argument="", EndText="", DebugName="";

    public LineFormat(){
    }
    
    public LineFormat(boolean isText, boolean isDependSep, String text, String translateText, String separated, String notSeparated, String function, String argument, String endText, String debugName) {
        this.isText = isText;
        this.isDependSep = isDependSep;
        Text = text;
        TranslateText = translateText;
        Separated = separated;
        NotSeparated = notSeparated;
        Function = function;
        Argument = argument;
        EndText = endText;
        DebugName = debugName;
    }

    public LineFormat(boolean isText, boolean isDependSep, String text, String separated, String notSeparated, String function, String argument, String endText, String debugName) {
        this(isText, isDependSep, text, "", separated, notSeparated, function, argument, endText, debugName);
    }

    public LineFormat(boolean isText, boolean isDependSep, String text, String separated, String notSeparated, String function, String argument, String debugName) {
        this( isText,  isDependSep,  text,  separated,  notSeparated,  function,  argument,  "",  debugName);

    }

    public LineFormat(boolean isText, String text, String separated, String notSeparated, String function, String argument, String debugName) {
        this(isText, false, text, separated, notSeparated, function, argument, debugName);
    }

    public LineFormat(boolean isText, String text, String separated, String notSeparated, String function, String argument) {
        this(isText, text, separated, notSeparated, function, argument, "");
    }

    public LineFormat(boolean isText, boolean isDependSep, String text, String separated, String notSeparated, String function, String argument) {
        this(isText, isDependSep, text, separated, notSeparated, function, argument, "");
    }

    public LineFormat(String text) {
        this(true, text, "", "", "", "");
    }

    public LineFormat(String text, String separated, String function) {
        this(false, text, separated, "", function, "");
    }

    public LineFormat(String text, String separated, String function, String argument) {
        this(false, text, separated, "", function, argument);
    }

}
