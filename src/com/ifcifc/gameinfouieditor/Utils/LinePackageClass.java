package com.ifcifc.gameinfouieditor.Utils;

import java.util.ArrayList;

public class LinePackageClass {
    public String name="", author="", description="";
    public ArrayList<LineClass> Lines;

    public LinePackageClass() {
        Lines = new ArrayList<>();
    }
    
    public LinePackageClass(String name, String author, String description, ArrayList<LineClass> Lines) {
        this.name = name;
        this.author = author;
        this.description = description;
        this.Lines = Lines;
    }

    
}
