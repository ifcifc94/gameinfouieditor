/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifcifc.gameinfouieditor;

import com.ifcifc.gameinfouieditor.Utils.DimensionFormat;
import com.ifcifc.gameinfouieditor.Utils.LineFormat;


public class Draw {
    
    public static void DrawLine(){
        try{
            DimensionFormat dim = Controller.getDim();

            String d="";
            boolean sep = false;
            for(LineFormat L : dim.Format){
                boolean it = !L.isText;
                if(sep && it)d += L.Separated;

                if(L.TranslateText.trim().isEmpty()){
                    d += L.Text;
                }else{
                    d += L.Text.replace("$Translated$", "<" + L.TranslateText + ">");
                }
                
                if(it){
                    d += "[" + L.Function + "]";
                    d += L.EndText;
                }
                sep = true;
            }

            if(Main.chk_mc_codes.isSelected()) d = d.replaceAll("§[0-9a-z]", "");

            Main.l_LineDraw.setText(d);
        }catch(Exception E){
        }
    }
    
}
