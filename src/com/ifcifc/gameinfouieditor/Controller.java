/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifcifc.gameinfouieditor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.ifcifc.gameinfouieditor.Utils.DimensionFormat;
import com.ifcifc.gameinfouieditor.Utils.Functions;
import com.ifcifc.gameinfouieditor.Utils.LineClass;
import com.ifcifc.gameinfouieditor.Utils.LineFormat;
import com.ifcifc.gameinfouieditor.Utils.LinePackageClass;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;


public class Controller {
    
    public static LinePackageClass LPC;
    public static Functions Fnc[];
    public static File lastPath = new File(".");
    public static void load(){
        //File file = new File("/home/igna/NetBeansProjects/JavaApplication1/defaultHUD.json");        
        
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
        fileChooser.setFileFilter(new FileNameExtensionFilter("json file","json"));
        if(lastPath != null)fileChooser.setCurrentDirectory(lastPath);
        
        if (fileChooser.showOpenDialog(fileChooser) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            lastPath = file;
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            LinePackageClass LP = LPC;
            try{
                LPC = gson.fromJson(new FileReader(file), LinePackageClass.class);
            }catch(JsonIOException | JsonSyntaxException | FileNotFoundException E){
                Controller.New();
            }
            
            update_package();
            update_lst_Lines();
            update_Line();
            
            Draw.DrawLine();
        }
        
    }
    
    public static void update_format_functions(){
        ArrayList<String> s = new ArrayList<>();
        for(Functions F : Fnc){
            s.add(F.function);
        }
        String[] toArray =  s.toArray(new String[0]);
        //Arrays.sort(toArray);
        Main.cb_format_function.setModel(new javax.swing.DefaultComboBoxModel<>(toArray));
    }
    
    public static void remove_lst_Lines(){
        String selectedValue = Main.lst_Lines.getSelectedValue();
        for(int i=0;i<LPC.Lines.size();i++){
            if(LPC.Lines.get(i).Name.equals(selectedValue)){
                LPC.Lines.remove(i);
                break;
            }
        }
        
        update_lst_Lines();
    }
    
    public static void add_lst_Lines(){
        String Base = "Line";
        int I = 1;
        while(checkLineName(Base+I)){
            I++;
        }
        
        Base += I;
        
        LPC.Lines.add(new LineClass(Base, 1));
        
        update_lst_Lines();
    }
    
    public static void add_lst_line_format(){
        DimensionFormat dim = getDim();      
        if(dim==null)return;
        dim.Format.add(new LineFormat(""));
        update_line_format();
    }
    
    public static void remove_lst_Lines_format(){
        int selectedValue = Main.lst_line_formats.getSelectedIndex();
        
        if(selectedValue<0)return;
        
        DimensionFormat dim = getDim();
        dim.Format.remove(selectedValue);

        Controller.update_line_format();
    }
    
    public static void clear_lst_Lines_format(){
        int selectedValue = Main.lst_line_formats.getSelectedIndex();
        
        if(selectedValue<0)return;
        
        DimensionFormat dim = getDim();
        dim.Format.clear();

        Controller.update_line_format();
    }
    
    public static void update_format(){
        DimensionFormat dim = getDim();
        int selectedIndex = Main.lst_line_formats.getSelectedIndex();
        
        if(selectedIndex<0)return;
        
        LineFormat get = dim.Format.get(selectedIndex);
        
        Main.chk_format_istext.setSelected(get.isText);
        Main.chk_format_sep.setSelected(get.isDependSep);
        Main.t_format_text.setText(get.Text);
        Main.t_format_arg.setText(get.Argument);
        Main.t_format_debugName.setText(get.DebugName);
        Main.t_format_endText.setText(get.EndText);
        Main.t_format_nsep.setText(get.NotSeparated);
        Main.t_format_sep.setText(get.Separated);
        Main.cb_format_function.setSelectedItem(get.Function);
        Main.t_format_Translate.setText(get.TranslateText);
        
        update_chk_formats();
    }
    
    public static void update_chk_formats(){
        boolean selected = !Main.chk_format_istext.isSelected();
        Main.chk_format_sep.setEnabled(selected);
        Main.t_format_endText.setEnabled(selected);
        Main.t_format_nsep.setEnabled(selected);
        Main.t_format_sep.setEnabled(selected);
        Main.cb_format_function.setEnabled(selected);
    }
    
    public static void update_lst_Lines(){
        ArrayList<String> lst_Lines_data = new ArrayList<>();
        LPC.Lines.forEach((L) -> {
            lst_Lines_data.add(L.Name);
        });
        Main.lst_Lines.setListData(lst_Lines_data.toArray(new String[0]));
        Main.lst_Lines.setSelectedIndex(0);
        
    }
    
    public static void update_package(){
        Main.t_Author.setText(LPC.author);
        Main.t_Description.setText(LPC.description);
        Main.t_Pkg_Name.setText(LPC.name);
    }
            
    public static void update_Line(){
        String selectedValue = Main.lst_Lines.getSelectedValue();
        LineClass L = getLine();
        try{
            Main.s_line_index.setValue(L.Index);
            Main.s_line_scalex.setValue(L.ScaleX);
            Main.s_line_scaley.setValue(L.ScaleY);
            Main.t_line_name.setText(L.Name);
            Main.chk_line_scale.setSelected(L.useDefaultScale);
        }catch(Exception e){
        }
        update_line_format();
        
    }
    
    public static void update_line_format(){
        if(LPC==null)return;
        Main.lst_line_formats.setListData(new String[]{});
        try{
            String selectedValue = Main.lst_Lines.getSelectedValue();
            DimensionFormat dim = getDim();

            ArrayList<String> Format = new ArrayList<>();

            for(int i=0; i<dim.Format.size();i++){
                Format.add("Format(" + i + "): " + dim.Format.get(i).DebugName);
            }

            Main.lst_line_formats.setListData(Format.toArray(new String[0]));
            Main.lst_line_formats.setSelectedIndex(0);        
            Draw.DrawLine();    
        }catch(Exception E){
        }
        
        
    }
    
    public static void loadFunction() throws FileNotFoundException{
        File file2 = new File("Packages.json");
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Fnc = gson.fromJson(new FileReader(file2), Functions[].class);
        
        update_format_functions();
    }
    
    public static void SaveFormat(){
        int selectedIndex = Main.lst_line_formats.getSelectedIndex();
        if(selectedIndex<0)return;        
        DimensionFormat dim = getDim();
        
        LineFormat get = dim.Format.get(selectedIndex);
        
        get.Argument = Main.t_format_arg.getText();
        get.DebugName = Main.t_format_debugName.getText();
        get.EndText = Main.t_format_endText.getText();
        get.Function = (String) Main.cb_format_function.getSelectedItem();
        get.NotSeparated = Main.t_format_nsep.getText();
        get.Separated = Main.t_format_sep.getText();
        get.Text = Main.t_format_text.getText();
        get.isDependSep = Main.chk_format_sep.isSelected();
        get.isText = Main.chk_format_istext.isSelected();
        get.TranslateText = Main.t_format_Translate.getText();
        
        int selectedIndex1 = Main.lst_line_formats.getSelectedIndex();
        update_line_format();
        Main.lst_line_formats.setSelectedIndex(selectedIndex);
        
        
        Draw.DrawLine();
    }
    
    public static void SaveLine(){
        LineClass L = getLine();
        
        if(L==null)return;
        
        L.Index = (int) Main.s_line_index.getValue();
        L.ScaleX = (float) Main.s_line_scalex.getValue();
        L.ScaleY = (float) Main.s_line_scaley.getValue();
        L.Name = Main.t_line_name.getText();
        L.useDefaultScale = Main.chk_line_scale.isSelected();
        
        update_lst_Lines();
        update_Line();
        
    }
    
    public static LineClass getLine(){
        String selectedValue = Main.lst_Lines.getSelectedValue();
        for(LineClass L : LPC.Lines){
            if(!L.Name.equals(selectedValue))continue;
            return L;
        }
        
        return null;
    }
    
    public static DimensionFormat getDim(){
        LineClass L = getLine();

        if(L == null)return null;

        String selectedItem = (String) Main.cb_line_dimension.getSelectedItem();
        try{
            for(DimensionFormat D : L.Line){
                if(!D.DimensionID.equals(selectedItem))continue;
                return D;
            }
        }catch(Exception E){
        }
        DimensionFormat dimensionFormat = new DimensionFormat(selectedItem);
        
        L.Line.add(dimensionFormat);
        
        return dimensionFormat;
    }
    
    private static boolean checkLineName(String Name){
        
        return LPC.Lines.stream().anyMatch((L) -> (L.Name.equals(Name)));
    }
    
    public static void checkFormatFunc(){
        int selectedIndex = Main.cb_format_function.getSelectedIndex();
        if(selectedIndex<0)return;
        Main.t_format_arg.setEnabled(Fnc[selectedIndex].arg);
    }
    
    
    public static void New(){
        LPC = new LinePackageClass();
        update_package();
        update_lst_Lines();
        update_Line();
    }
    
    private static void clearJSON(){
        ArrayList<LineClass> Lc = new ArrayList<>();
        
        LPC.Lines.forEach(L->{
            ArrayList<DimensionFormat> _D =  new ArrayList<>();
            
            L.Line.forEach(D ->{
                if(D.Format.isEmpty()){
                    _D.add(D);
                }
            });
            
            _D.forEach(L.Line::remove);
        });
        
        LPC.Lines.stream().filter((L) -> (L.Line.isEmpty())).forEachOrdered((L) -> {
            Lc.add(L);
        });
        Lc.forEach(LPC.Lines::remove);
    }
    
    public static void SaveFile(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
        fileChooser.setFileFilter(new FileNameExtensionFilter("json file","json"));
        
        Controller.LPC.author = Main.t_Author.getText();
        Controller.LPC.description = Main.t_Description.getText();
        Controller.LPC.name = Main.t_Pkg_Name.getText();
        
        if(lastPath != null)fileChooser.setCurrentDirectory(lastPath);

        if (fileChooser.showOpenDialog(fileChooser) == JFileChooser.APPROVE_OPTION) {
            clearJSON();
            update_package();
            update_lst_Lines();
            update_Line();
            File file = fileChooser.getSelectedFile();
            
            lastPath = file;
            
            
            if(!file.getAbsolutePath().contains(".json")){
                file = new File(file.getAbsolutePath()+".json");
            }
            
            try {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                FileWriter sFile;
                sFile = new FileWriter(file);
                sFile.write(gson.toJson(LPC));
                sFile.close();
            } catch (IOException e) {
            } 
        }

    }
    
}
